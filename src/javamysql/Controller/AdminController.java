/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javamysql.Controller;

import javamysql.database.ICrudImpl;
import javamysql.model.User;

/**
 *
 * @author Filippo-TheAppExpert
 */
public class AdminController {

    private static AdminController controller;
    private final ICrudImpl iCrudImpl;
    
    private AdminController() {
        this.iCrudImpl = new ICrudImpl();
        this.iCrudImpl.openConnection();
    }

    public static AdminController getController() {
        if (controller == null) {
            controller = new AdminController();
        }
        return controller;
    }

    public boolean signup(User user) {
        return this.iCrudImpl.insert(user);
    }

    public User login(String username, String password) {
        return this.iCrudImpl.getUser(username, password);
    }
}
